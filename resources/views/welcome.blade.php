<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hypertube</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            * {
                padding: 0;
                margin: 0;
                box-sizing: border-box;
                font-family: 'Nunito', sans-serif;
            }
            html, body, .main {
                height: 100%;
            }
            .main {
                background-color: #f0f0f0;
                display: flex;
                flex-direction: column;
                justify-content: space-between;
            }
            /*<!-- HEADER -->*/
            .header {
                height: 90px;
                color: white;
                background-color: #e8ae68;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .button-home {
                margin-left: 10px;
                text-decoration: none;
                font-family: 'Nunito', sans-serif;
                font-size: 30px;
                font-weight: bold;
                color: #f0f0f0;
            }
            .header-links ul {
                display: flex;
                list-style: none;
            }
            .header-links a {
                color: #f0f0f0;
                margin-right: 20px;
                padding: 10px;
                border: 1px solid #f0f0f0;
                border-radius: 5px;
                text-decoration: none;
            }
            .header-links a:hover {
                background-color: #f0f0f0;
                color: #e8ae68;
            }

            .content {
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .content-button {
                text-decoration: none;
                text-align: center;
                font-weight: bold;
                color: #e8ae68;
            }
            .content-button:hover {
                text-decoration: underline;
            }
            .content-button-main {
                text-decoration: none;
                padding: 15px 0;
                border: 1px solid #e8ae68;
                border-radius: 10px;
                text-align: center;
                font-size: 20px;
                font-weight: bold;
                margin: 10px;
                color: #e8ae68;
                max-width: 600px;
                width: 90%;
            }
            .content-button-main:hover {
                background-color: #e8ae68;
                color: #f0f0f0;
            }
            .content p {
                text-align: center;
            }
            .content p:first-of-type {
                font-size: 20px;
            }

            .title {
                color: #e8ae68;
                font-size: 50px;
                padding: 50px 0;
                text-align: center;
            }
            .title span {
                color: #db5a42;
            }
            .footer {
                width: 100%;
                background-color: #a57f60;
                align-self: flex-start;
                text-align: center;
                color: #f0f0f0;
                padding: 5px;
            }

        </style>
    </head>
    <body>

        <div class="main">
            <header class="header">
                <a class="button-home" href="#">Hypertube</a>
                <div class="header-links">
                    <ul>
                        <li><a href="#">Log In</a></li>
                        <li><a href="#">Sign Up</a></li>
                    </ul>
                </div>
            </header>

            <section class="content">
                <h1 class="title">Welcome to <span>Hypertube</span>!</h1>
                <a class="content-button-main" href="#">Create an Account</a>
                <p>Already have one? <a class="content-button" href="#">Log In</a></p>
            </section>

            <footer class="footer">
                <p>Made by <a class="content-button" href="https://github.com/dp97">duemti</a></p>
            </footer>
        </div>

    </body>
</html>
